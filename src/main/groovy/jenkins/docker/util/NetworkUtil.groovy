package jenkins.docker.util

class NetworkUtil {
    static def withDockerNetwork(String networkId, Closure inner) {
        try {
            "docker network create ${networkId}".execute().waitFor()
            inner.call()
        } finally {
            "docker network rm ${networkId}".execute().waitFor()
        }
    }

    static def withContainerConnected(String network, String container, Closure inner) {
        try {
            "docker network connect ${network} ${container}".execute().waitFor()
            inner.call()
        } finally {
            "docker network disconnect ${network} ${container}".execute().waitFor()
        }
    }
}